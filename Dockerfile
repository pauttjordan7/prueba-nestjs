FROM node:20-alpine3.18 AS base

WORKDIR /app
ARG NPM_TOKEN

FROM base AS dev

ENV NODE_ENV=development

COPY package*.json ./

RUN echo "//registry.npmjs.org/:_authToken=$NPM_TOKEN" > ".npmrc" && \
    npm ci && \
    rm -f .npmrc

COPY . .

EXPOSE $PORT
CMD ["npm", "run", "start:dev"]

# Etapa de construcción
FROM base AS build

COPY package*.json ./
RUN echo "//registry.npmjs.org/:_authToken=$NPM_TOKEN" > ".npmrc" && \
    npm install --save-optional \
        "@swc/core-linux-x64-gnu@1" \
        "@swc/core-linux-x64-musl@1" && \
    rm -f .npmrc

COPY . .

RUN npm run build && \
    npm prune --production

FROM base AS production

RUN apk update && apk add --no-cache dumb-init=1.2.5-r2

ENV NODE_ENV=production

WORKDIR /app
COPY --from=build /app/node_modules ./node_modules
COPY --from=build /app/dist ./dist

USER node
EXPOSE $PORT
CMD ["dumb-init", "node", "dist/main.js"]
