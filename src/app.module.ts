import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ProductModule } from './modules/product/product.module';
import configuration from './config/configuration';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeOrmConfigAsync } from './config/typeorm';
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { LoggerInterceptor } from './common/middleware/log.interceptor';
import { ResponseInterceptor } from './common/middleware/response.interceptor';
import { CatchInterceptor } from './common/middleware/catch.interceptor';
import { AuthModule } from './modules/auth/auth.module';
import { AuthGuard } from './modules/auth/guard/guard.guard';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      cache: true,
      load: [configuration],
    }),
    TypeOrmModule.forRootAsync(TypeOrmConfigAsync),
    ProductModule,
    AuthModule,
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: CatchInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: ResponseInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggerInterceptor,
    },
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
  ],
})
export class AppModule {}
