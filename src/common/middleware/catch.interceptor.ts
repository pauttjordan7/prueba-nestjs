import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';
import { Response } from 'express';

@Catch()
export class CatchInterceptor implements ExceptionFilter {
  constructor(private readonly httpAdapterHost: HttpAdapterHost) {}

  catch(exception: any, host: ArgumentsHost) {
    const { httpAdapter } = this.httpAdapterHost;

    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    const responseBody = {
      message: (exception as any).response?.message ?? exception.message,
      code: (exception as any).response?.error ?? 'Internal Error',
      status: (exception as any).status ?? 500,
    };

    httpAdapter.reply(response, responseBody, responseBody.status);
  }
}
