import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserDto } from './dto/create-user.dto';
import { ApiTags } from '@nestjs/swagger';
import { IUser } from './interface/user.interface';
import { LoginDto } from './dto/login.dto';
import { Public } from 'src/common/decorators/public.decorator';

@Public()
@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('create-user')
  createUser(@Body() user: UserDto): Promise<IUser> {
    return this.authService.createUser(user);
  }

  @Post('login')
  login(@Body() userCredential: LoginDto) {
    return this.authService.login(userCredential);
  }
}
