import { Injectable, UnauthorizedException } from '@nestjs/common';
import { User } from './entity/user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import {
  IUser,
  IUserCreate,
  IUserCredential,
} from './interface/user.interface';

import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User)
    private productRepository: Repository<User>,
    private jwtService: JwtService,
  ) {}

  async createUser(user: IUserCreate): Promise<IUser> {
    const password = await bcrypt.hash(user.password, 10);
    return this.productRepository.save({
      ...user,
      password,
    });
  }

  async login(userCredential: IUserCredential) {
    const user = await this.productRepository.findOneBy({
      username: userCredential.username,
    });

    if (!user) {
      throw new UnauthorizedException('Usuario no encontrado');
    }

    const isMatch = await bcrypt.compare(
      userCredential.password,
      user.password,
    );

    if (!isMatch) {
      throw new UnauthorizedException('Contraseña incorrecta');
    }

    const payload = { sub: user.id, username: user.username };

    return { ...user, access_token: await this.jwtService.signAsync(payload) };
  }
}
