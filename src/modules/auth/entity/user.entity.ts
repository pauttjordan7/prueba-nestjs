import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'users' })
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    nullable: false,
    length: 200,
    comment: 'Nombre del usuario',
  })
  fullName: string;

  @Column({
    nullable: false,
    length: 200,
    comment: 'usuario login',
  })
  username: string;

  @Column({
    nullable: false,
    length: 1000,
    comment: 'contraseña login',
  })
  password: string;
}
