export interface IUser {
  id: number;
  fullName: string;
  username: string;
  password: string;
}

export interface IUserCreate extends Omit<IUser, 'id'> {}

export interface IUserCredential {
  username: string;
  password: string;
}
