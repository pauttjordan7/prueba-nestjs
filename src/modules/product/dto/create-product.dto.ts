import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsNumber,
  IsOptional,
  MaxLength,
  IsPositive,
  IsIn,
} from 'class-validator';

export class CreateProductDto {
  @ApiProperty({
    required: true,
    maxLength: 200,
    example: 'Computador',
    description: 'Nombre del producto',
  })
  @IsString()
  @MaxLength(200)
  name: string;

  @ApiProperty({
    required: false,
    maxLength: 1000,
    description: 'Descripcion del producto',
  })
  @IsString()
  @IsOptional()
  @MaxLength(1000)
  description: string;

  @ApiProperty({
    required: true,
    example: 1000,
    description: 'Precio del producto',
  })
  @IsNumber()
  @IsPositive({ message: 'el precio debe ser un número positivo' })
  price: number;

  @ApiProperty({
    required: true,
    example: 1,
    description: 'Cantidad del producto',
  })
  @IsNumber()
  @IsPositive({ message: 'la cantidad debe ser un número positivo' })
  amount: number;

  @ApiProperty()
  @IsIn(['A', 'B'])
  address: 'A' | 'B';
}
