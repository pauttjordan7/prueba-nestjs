import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'products' })
export class Product {
  @ApiProperty({
    description: '$id producto',
  })
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({
    description: 'Nombre del producto',
    example: 'Computador',
  })
  @Column({
    nullable: false,
    length: 200,
    comment: 'Nombre del producto',
  })
  name: string;

  @ApiProperty({
    required: false,
    example: 'computador',
    description: 'Descripción del producto',
  })
  @Column({
    nullable: true,
    length: 1000,
    comment: 'Descripción del producto',
  })
  description: string;

  @ApiProperty({
    example: 1000,
  })
  @Column({
    nullable: false,
    type: 'decimal',
    comment: 'Precio del producto',
  })
  price: number;

  @ApiProperty({
    example: 1,
  })
  @Column({
    nullable: false,
    type: 'int',
    comment: 'Cantidad del producto',
  })
  amount: number;

  @Column({
    default: 'A',
    enum: ['A', 'B'],
  })
  address: 'A' | 'B';

  @ApiProperty()
  @CreateDateColumn()
  created_at: Date;

  @ApiProperty()
  @UpdateDateColumn()
  updated_at: Date;
}
