export interface IProduct {
  id: number;
  created_at: Date;
  updated_at: Date;
  name: string;
  description: string;
  price: number;
  amount: number;
  address: 'A' | 'B';
}

export interface IProductCreate
  extends Omit<IProduct, 'id' | 'created_at' | 'updated_at'> {}

export interface IProductUpdate extends Partial<IProductCreate> {}
