import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Put,
  Query,
} from '@nestjs/common';
import { ProductService } from './product.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { IProduct } from './interfaces/product.interface';
import { Product } from './entities/product.entity';
import { IPagination } from './interfaces/pagination.interface';

@ApiBearerAuth()
@ApiTags('Productos')
@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @ApiCreatedResponse({
    description: 'Operador creado.',
    type: Product,
  })
  @Post()
  create(@Body() createProductDto: CreateProductDto): Promise<IProduct> {
    return this.productService.create(createProductDto);
  }

  @ApiResponse({
    status: 201,
    type: [Product],
  })
  @Get()
  findAll(@Query() params: IPagination): Promise<IProduct[]> {
    console.log(params);

    return this.productService.findAll(params);
  }

  @ApiResponse({
    status: 201,
    type: Product,
  })
  @Get(':id')
  findOne(@Param('id') id: string): Promise<IProduct> {
    return this.productService.findOne({
      id: Number(id),
    });
  }

  @ApiResponse({
    status: 201,
    type: Product,
  })
  @Get('name/:name')
  findByName(@Param('name') name: string): Promise<IProduct> {
    return this.productService.findOne({
      name,
    });
  }

  @ApiNotFoundResponse({
    description: 'Producto no encontrado',
  })
  @ApiResponse({
    status: 201,
    type: Product,
  })
  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateProductDto: UpdateProductDto,
  ): Promise<IProduct> {
    return this.productService.update(+id, updateProductDto);
  }

  @ApiNotFoundResponse({
    description: 'Producto no encontrado',
  })
  @ApiResponse({
    status: 201,
    type: Product,
  })
  @Delete(':id')
  remove(@Param('id') id: string): Promise<IProduct> {
    return this.productService.remove(+id);
  }
}
