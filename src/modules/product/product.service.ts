import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';
import {
  IProduct,
  IProductCreate,
  IProductUpdate,
} from './interfaces/product.interface';
import { IPagination } from './interfaces/pagination.interface';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
  ) {}

  create(product: IProductCreate): Promise<IProduct> {
    return this.productRepository.save({
      ...product,
    });
  }

  findAll({ page, limit }: IPagination): Promise<IProduct[]> {
    return this.productRepository.find({
      order: { id: 'DESC' },
      skip: +page,
      take: +limit,
    });
  }

  async findOne(params: { id?: number; name?: string }): Promise<IProduct> {
    const result = await this.productRepository.findOneBy({ ...params });

    if (!result) {
      throw new NotFoundException('Producto no encontrado');
    }

    return result;
  }

  async update(id: number, productUpdate: IProductUpdate): Promise<any> {
    const isEmptyProduct = await this.productRepository.findOneBy({ id });

    if (!isEmptyProduct) {
      throw new NotFoundException('Producto no encontrado');
    }

    await this.productRepository.update(id, productUpdate);
    return await this.productRepository.findOneBy({ id });
  }

  async remove(id: number): Promise<IProduct> {
    const product = await this.productRepository.findOneBy({ id });

    if (!product) {
      throw new NotFoundException('Producto no encontrado');
    }

    await this.productRepository.delete(id);
    return product;
  }
}
